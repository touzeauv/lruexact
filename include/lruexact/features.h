#ifndef LRUEXACT_FEATURES_H_
#define LRUEXACT_FEATURES_H_

#include <otawa/proc/AbstractFeature.h>

namespace lruexact {

enum class RefinementCategory
{
	CLASSIFIED,
	AH_CANDIDATE,
	AM_CANDIDATE,
	AH_AM_CANDIDATE
};

extern otawa::p::feature REFINEMENT_CATEGORY_FEATURE;
//hook: access
extern otawa::p::id<RefinementCategory> REFINEMENT_CATEGORY;




enum class ExactCategory
{
	AH,
	AM,
	DU,
	NC
};

extern otawa::p::feature LRU_CLASSIFICATION_FEATURE;

extern otawa::p::id<ExactCategory> EXACT_CATEGORY;
extern otawa::p::id<bool> CLASSIFICATION_TO_FILE;
extern otawa::p::id<elm::sys::Path> CLASSIFICATION_PATH;



} // namespace lruexact

#endif // LRUEXACT_FEATURES_H
