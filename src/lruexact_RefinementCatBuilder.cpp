#include <lruexact/features.h>

#include <otawa/icache/features.h>
#include <otawa/icat3/features.h>
#include <otawa/proc/BBProcessor.h>

#include <lrupreanalysis/features.h>

using namespace otawa;

namespace lruexact
{

class RefinementCatBuilder : public BBProcessor
{
public:
	static otawa::p::declare reg;

	RefinementCatBuilder() :
		otawa::BBProcessor(reg),
		_hasICat(false),
		_hasDU(false)
	{
	}

protected:

	virtual void setup(WorkSpace* ws) override
	{
		_hasICat = ws->isProvided(icat3::CATEGORY_FEATURE);
		_hasDU = ws->isProvided(lrupreanalysis::eh_em::DU_CATEGORY_FEATURE);
	}

	virtual void processBB(WorkSpace*, CFG*, Block* b) override
	{
		if(!b->isBasic())
			return;

		BasicBlock* bb = b->toBasic();

		for(Block::EdgeIter edgeIter(bb->ins()); edgeIter; ++edgeIter) {
			Edge* e = *edgeIter;
			Bag<icache::Access>& bag = icache::ACCESSES(e).ref();
			processBag(bag);
		}

		Bag<icache::Access>& bag = icache::ACCESSES(bb).ref();
		processBag(bag);
	}

	void processBag(Bag<icache::Access>& bag)
	{
		for(int i = 0; i < bag.size(); ++i)
			processAccess(&bag[i]);
	}

	void processAccess(icache::Access* access)
	{
		RefinementCategory c = RefinementCategory::AH_AM_CANDIDATE;
		lruexact::ExactCategory e = lruexact::ExactCategory::NC;

		if(_hasDU) {
			lrupreanalysis::eh_em::DUCategory ducat = lrupreanalysis::eh_em::DU_CATEGORY(access);

			if(ducat == lrupreanalysis::eh_em::DUCategory::DU){
				c = RefinementCategory::CLASSIFIED;
				e = lruexact::ExactCategory::DU;
			}
			else if(ducat == lrupreanalysis::eh_em::DUCategory::EH){
				c = RefinementCategory::AH_CANDIDATE;
			}
			else if(ducat == lrupreanalysis::eh_em::DUCategory::EM){
				c = RefinementCategory::AM_CANDIDATE;
			}
		}
		if(_hasICat) {
			icat3::category_t cat = icat3::CATEGORY(access);
			if(cat == icat3::AH) {
				c = RefinementCategory::CLASSIFIED;
				e = lruexact::ExactCategory::AH;
			}
			else if(cat == icat3::AM){
				c = RefinementCategory::CLASSIFIED;
				e = lruexact::ExactCategory::AM;
			}
		}

		if(logFor(LOG_BLOCK)) {
			switch(c) {
				case RefinementCategory::CLASSIFIED:
					log << "\t\t\tAccess " << *access << " is CLASSIFIED" << io::endl;
					break;
				case RefinementCategory::AH_CANDIDATE:
					log << "\t\t\tAccess " << *access << " is AH_CANDIDATE" << io::endl;
					break;
				case RefinementCategory::AM_CANDIDATE:
					log << "\t\t\tAccess " << *access << " is AM_CANDIDATE" << io::endl;
					break;
				case RefinementCategory::AH_AM_CANDIDATE:
					log << "\t\t\tAccess " << *access << " is AH_AM_CANDIDATE" << io::endl;
					break;
			}
		}
		REFINEMENT_CATEGORY(access) = c;
		lruexact::EXACT_CATEGORY(access) = e;
	}

	bool _hasICat;
	bool _hasDU;
};

p::declare RefinementCatBuilder::reg = p::init("lruexact::RefinementCatBuilder", Version(1, 0, 0))
	.require(icache::ACCESSES_FEATURE)
//	.require(icat3::CATEGORY_FEATURE)
//	.require(DU_CATEGORY_FEATURE)
	.provide(REFINEMENT_CATEGORY_FEATURE)
	.make<RefinementCatBuilder>();

p::feature REFINEMENT_CATEGORY_FEATURE("lruexact::REFINEMENT_CATEGORY_FEATURE", p::make<RefinementCatBuilder>());

otawa::p::id<RefinementCategory> REFINEMENT_CATEGORY("lruexact::REFINEMENT_CATEGORY");

} // namespace lruexact

