#include <lruexact/features.h>

#include <otawa/icache/features.h>
#include <otawa/icat3/features.h>
#include <otawa/proc/BBProcessor.h>

using namespace otawa;

namespace lruexact
{

class DefaultClassificationBuilder : public BBProcessor
{
public:
	static otawa::p::declare reg;

	DefaultClassificationBuilder() : otawa::BBProcessor(reg)
	{
	}

protected:

	virtual void processBB(WorkSpace*, CFG*, Block* b) override
	{
		if(!b->isBasic())
			return;

		BasicBlock* bb = b->toBasic();

		for(Block::EdgeIter edgeIter(bb->ins()); edgeIter; ++edgeIter) {
			Edge* e = *edgeIter;
			Bag<icache::Access>& bag = icache::ACCESSES(e).ref();
			processBag(bag);
		}

		Bag<icache::Access>& bag = icache::ACCESSES(bb).ref();
		processBag(bag);
	}

	void processBag(Bag<icache::Access>& bag)
	{
		for(int i = 0; i < bag.size(); ++i)
			processAccess(&bag[i]);
	}

	void processAccess(icache::Access* access)
	{
		EXACT_CATEGORY(access) = ExactCategory::NC;
	}
};

p::declare DefaultClassificationBuilder::reg = p::init("lruexact::DefaultClassificationBuilder", Version(1, 0, 0))
	.require(icache::ACCESSES_FEATURE)
	.provide(LRU_CLASSIFICATION_FEATURE)
	.make<DefaultClassificationBuilder>();

p::feature LRU_CLASSIFICATION_FEATURE("lruexact::LRU_CLASSIFICATION_FEATURE", p::make<DefaultClassificationBuilder>());

} // namespace lruexact

