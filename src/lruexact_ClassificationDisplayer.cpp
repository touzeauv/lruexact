#include <lruexact/features.h>

#include <elm/sys/Path.h>
#include <elm/sys/System.h>

#include <otawa/icache/features.h>
#include <otawa/icat3/features.h>
#include <otawa/proc/BBProcessor.h>
#include <otawa/prog/Process.h>

using namespace otawa;

namespace lruexact
{

class ClassificationDisplayer : public BBProcessor
{
public:
	static otawa::p::declare reg;

	ClassificationDisplayer() :
		otawa::BBProcessor(reg),
		_path(""),
		_toFile(false),
		_stream(nullptr),
		_line(false)
	{
	}

	virtual void configure(const PropList& props) override
	{
		otawa::BBProcessor::configure(props);
		if(props.hasProp(CLASSIFICATION_PATH))
			_path = CLASSIFICATION_PATH(props);
		if(props.hasProp(CLASSIFICATION_TO_FILE))
			_toFile = CLASSIFICATION_TO_FILE(props);
	}
protected:

	virtual void setup(WorkSpace *ws) override
	{
		// Check if source is available
		if(ws->isProvided(SOURCE_LINE_FEATURE))
			_line = true;
		if(logFor(LOG_PROC))
			log << "\tsource/line information " << (_line ? "" : "not ") << "available\n";

		if(!_path && _toFile)
			_path = _ << "access_classification.txt";

		if(_path) {
			try {
				_stream = elm::sys::System::createFile(_path);
			}
			catch(elm::sys::SystemException& e) {
				throw ProcessorException(*this, _ << "cannot open \"" << _path << "\"");
			}

			_out.setStream(*_stream);
		}
		_out << "ACCESS\t\tKIND\tCATEGORY\tBB\t";
		if(_line)
			_out << "\tLINE";

		_out << io::endl;
	}

	virtual void processCFG(WorkSpace* ws, CFG* cfg) override
	{
		_out << "FUNCTION " << cfg->label() << io::endl;
		BBProcessor::processCFG(ws, cfg);
	}

	virtual void processBB(WorkSpace* ws, CFG* cfg, Block* b) override
	{
		if(!b->isBasic())
			return;

		BasicBlock* bb = b->toBasic();

		for(Block::EdgeIter edgeIter(bb->ins()); edgeIter; ++edgeIter) {
			const Edge* e = *edgeIter;
			const Bag<icache::Access>& bag = icache::ACCESSES(e);
			processBag(ws, cfg, bag, bb);
		}

		const Bag<icache::Access>& bag = icache::ACCESSES(bb);
		processBag(ws, cfg, bag, bb);
	}

	void processBag(WorkSpace* ws, CFG* cfg, const Bag<icache::Access>& bag, BasicBlock* bb)
	{
		for(int i = 0; i < bag.size(); ++i)
			processAccess(ws, cfg, &bag[i], bb);
	}

	void processAccess(WorkSpace* ws, CFG*, const icache::Access* access, BasicBlock* bb)
	{
		_out << access->address() << "\t";
		switch(access->kind()){
			case icache::NONE: _out << "NONE\t"; break;
			case icache::FETCH: _out << "FETCH\t"; break;
			case icache::PREFETCH: _out << "PREFETCH\t"; break;
		}
		ExactCategory cat = EXACT_CATEGORY(access);

		if(cat == ExactCategory::AH)
			_out << "AH\t\t";
		else if(cat == ExactCategory::AM)
			_out << "AM\t\t";
		else if(cat == ExactCategory::DU)
			_out << "DU\t\t";
		else
			_out << "NC\t\t";
		_out << "BB " << bb->index() << "\t\t";
		printLine(ws, bb->address(), bb->topAddress().offset());
		_out << io::endl;
	}

	void printLine(WorkSpace* ws, Address begin, Address::offset_t offset)
	{

		// Display line
		if(_line) {
			bool one = false;
			Pair<cstring, int> old = pair(cstring(""), 0);
			for(Address addr = begin; addr.offset() < offset; addr = addr + 1) {
				Option<Pair<cstring, int> > res = ws->process()->getSourceLine(addr);
				if(res) {
					if((*res).fst != old.fst) {
						old = *res;
			 			if(one)
			 				_out << ", ";
			 			else
			 				one = true;
			 			_out << old.fst << ':' << old.snd;
					}
					else if((*res).snd != old.snd) {
						old = *res;
						_out << ',' << old.snd;
					}
				}
			}
		}
	}

	virtual void cleanup(WorkSpace*) override
	{
		if(_stream)
			delete _stream;
	}

	elm::io::Output _out;
	elm::sys::Path _path;
	bool _toFile;
	elm::io::OutStream* _stream;
	bool _line;
};

p::declare ClassificationDisplayer::reg = p::init("lruexact::ClassificationDisplayer", Version(1, 0, 0))
	.require(LRU_CLASSIFICATION_FEATURE)
	.make<ClassificationDisplayer>();


p::id<bool> CLASSIFICATION_TO_FILE("lruexact::CLASSIFICATION_TO_FILE", false);
p::id<elm::sys::Path> CLASSIFICATION_PATH("lruexact::CLASSIFICATION_PATH", "/tmp/");

} // namespace lruexact

