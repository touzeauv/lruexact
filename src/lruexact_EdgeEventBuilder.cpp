/*
 *	icat3::EdgEventBuilder class implementation
 *	Copyright (c) 2016, IRIT UPS.
 *
 *	This file is part of OTAWA
 *
 *	OTAWA is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	OTAWA is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with OTAWA; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 *	02110-1301  USA
 */

#include <lruexact/features.h>

#include <otawa/cfg.h>
#include <otawa/etime/features.h>
#include <otawa/hard/Memory.h>
#include <otawa/icache/features.h>
#include <otawa/icat3/features.h>
#include <otawa/ipet.h>
#include <otawa/proc/Processor.h>
#include <otawa/program.h>

using namespace otawa;

namespace lruexact
{

io::Output& operator<<(io::Output& out, ExactCategory cat) {
	switch(cat) {
		case ExactCategory::AH:	out << "AH"; break;
		case ExactCategory::AM:	out << "AM"; break;
		case ExactCategory::DU:	out << "DU"; break;
		case ExactCategory::NC:	out << "NC"; break;
	}
	return out;
}

/**
 */
class ICacheEvent: public etime::Event {
public:
	ICacheEvent(const PropList* owner,
	            const icache::Access& acc,
	            ExactCategory cat,
	            ot::time cost) :
		Event(acc.instruction()),
		_owner(owner),
		_acc(acc),
		_cat(cat),
		_cost(cost),
		_type(etime::LOCAL),
		_rel(pair(null<Inst>(), null<const hard::PipelineUnit>()))
	{
	}

	virtual etime::kind_t kind(void) const
	{
		return etime::FETCH;
	}

	virtual ot::time cost(void) const
	{
		return _cost;
	}

	virtual etime::type_t type(void) const
	{
		return _type;
	}

	virtual etime::occurrence_t occurrence(void) const
	{
		switch(_cat) {
		case ExactCategory::AH:	return etime::NEVER;
		case ExactCategory::AM:	return etime::ALWAYS;
		case ExactCategory::DU:	return etime::SOMETIMES;
		case ExactCategory::NC:	return etime::SOMETIMES;
		default:	ASSERT(0); return etime::SOMETIMES;
		}
	}

	virtual cstring name(void) const
	{
		return "L1 instruction cache miss";
	}

	virtual string detail(void) const
	{
		return _ << _cat << " access to " << _acc.address();
	}

//	virtual int weight(void) const {
//		switch(_cat) {
//		case ExactCategory::AH: return 0;
//		case ExactCategory::AM:
//		case ExactCategory::DU:
//		case ExactCategory::NC: return WEIGHT(_owner);
//		default: ASSERT(0);
//		}
//	}

	virtual rel_t related(void) const { return _rel; }
	virtual inline void relate(const rel_t &rel) { _rel = rel; }
	virtual inline void setType(etime::type_t type) { _type = type; }

private:
	const PropList *_owner;
	const icache::Access& _acc;
	ExactCategory _cat;
	ot::time _cost;
	etime::type_t _type;
	rel_t _rel;
};


/**
 */
class EdgeEventBuilder: public BBProcessor {
public:
	static p::declare reg;
	EdgeEventBuilder(void):
		BBProcessor(reg),
		coll(0),
		mem(0)
	{
	}

protected:

	static const bool PREFIX = true, BLOCK = false;

	virtual void setup(WorkSpace *ws) {
		coll = icat3::LBLOCKS(ws);
		ASSERT(coll);
		mem = hard::MEMORY(ws);
		ASSERT(mem);
	}


	virtual void processBB(WorkSpace*, CFG*, Block* block)
	{

		if(!block->isBasic())
			return;
		BasicBlock* bb = block->toBasic();

		for(Block::EdgeIter edgeIter(bb->ins()); edgeIter; ++edgeIter) {
			Edge* e = *edgeIter;
			const Bag<icache::Access>& bag = icache::ACCESSES(e);
			processAccesses(e, bag);
		}

		const Bag<icache::Access>& bag = icache::ACCESSES(bb);
		processAccesses(bb, bag);
	}

	void processAccesses(PropList* prop, const Bag<icache::Access>& accs)
	{
		for(int i = 0; i < accs.count(); i++) {

			icat3::LBlock* const lb = icat3::LBLOCK(accs[i]);
			ASSERT(lb);


			ot::time cost = 0;
			const hard::Bank *bank = mem->get(lb->address());
			if(!bank)
				cost = mem->worstReadTime();
			else
				cost = bank->readLatency();

			ExactCategory cat = EXACT_CATEGORY(accs[i]);
			// build the events
			etime::EVENT(prop).add(new ICacheEvent(prop, accs[i], cat, cost));
		}
	}

	const icat3::LBlockCollection *coll;
	const hard::Memory *mem;
};

p::declare EdgeEventBuilder::reg = p::init("lruexact::EdgeEventBuilder", Version(1, 0, 0))
	.extend<BBProcessor>()
	.make<EdgeEventBuilder>()
	.require(icat3::LBLOCKS_FEATURE)
	.require(LRU_CLASSIFICATION_FEATURE)
	.require(hard::MEMORY_FEATURE)
	.require(COLLECTED_CFG_FEATURE)
	.provide(etime::EVENTS_FEATURE)
	.provide(icat3::EDGE_EVENTS_FEATURE);

} // namespace lruexact

