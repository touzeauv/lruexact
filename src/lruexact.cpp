#include <otawa/proc/ProcessorPlugin.h>

#include <lruexact/features.h>

namespace lruexact {

using namespace elm;
using namespace otawa;

class Plugin: public ProcessorPlugin {
public:
	Plugin(void): ProcessorPlugin("lruexact", Version(1, 0, 0), OTAWA_PROC_VERSION) { }
};

p::id<ExactCategory> EXACT_CATEGORY("lruexact::EXACT_CATEGORY");

} // namespace lruexact

lruexact::Plugin lruexact_plugin;
ELM_PLUGIN(lruexact_plugin, OTAWA_PROC_HOOK);

