#include <lruexact/features.h>

#include <otawa/icache/features.h>
#include <otawa/icat3/features.h>
#include <otawa/proc/BBProcessor.h>

using namespace otawa;

namespace lruexact
{

class MayMustClassificationBuilder : public BBProcessor
{
public:
	static otawa::p::declare reg;

	MayMustClassificationBuilder() : otawa::BBProcessor(reg)
	{
	}

protected:

	virtual void processBB(WorkSpace*, CFG*, Block* b) override
	{
		if(!b->isBasic())
			return;

		BasicBlock* bb = b->toBasic();

		for(Block::EdgeIter edgeIter(bb->ins()); edgeIter; ++edgeIter) {
			Edge* e = *edgeIter;
			Bag<icache::Access>& bag = icache::ACCESSES(e).ref();
			processBag(bag);
		}

		Bag<icache::Access>& bag = icache::ACCESSES(bb).ref();
		processBag(bag);
	}

	void processBag(Bag<icache::Access>& bag)
	{
		for(int i = 0; i < bag.size(); ++i)
			processAccess(&bag[i]);
	}

	void processAccess(icache::Access* access)
	{
		ExactCategory cat;
		switch(icat3::CATEGORY(access)) {
			case icat3::AH:
				cat = ExactCategory::AH;
				break;
			case icat3::AM:
				cat = ExactCategory::AM;
				break;
			default:
				cat = ExactCategory::NC;
		}
		EXACT_CATEGORY(access) = cat;
	}
};

p::declare MayMustClassificationBuilder::reg = p::init("lruexact::MayMustClassificationBuilder", Version(1, 0, 0))
	.require(icache::ACCESSES_FEATURE)
	.require(icat3::CATEGORY_FEATURE)
	.provide(LRU_CLASSIFICATION_FEATURE)
	.make<MayMustClassificationBuilder>();

} // namespace lruexact

